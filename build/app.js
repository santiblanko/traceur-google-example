"use strict";
var __moduleName = (void 0);
var Greeter = function Greeter(message) {
  this.message = message;
};
($traceurRuntime.createClass)(Greeter, {greet: function() {
    var element = document.querySelector('#message');
    element.innerHTML = this.message;
  }}, {});
;
var greeter = new Greeter('Hola, Mundo!');
greeter.greet();
var fs = [];
for (var i = 0; i < 10; i++) {
  fs.push(function() {
    console.log(i);
  });
}
;
fs.forEach(function(f) {
  f();
});
var fs = [];
{
  try {
    throw undefined;
  } catch ($i) {
    $i = 0;
    for (; $i < 10; $i++) {
      try {
        throw undefined;
      } catch (i) {
        i = $i;
        try {
          fs.push(function() {
            console.log(i);
          });
        } finally {
          $i = i;
        }
      }
    }
  }
}
;
fs.forEach(function(f) {
  f();
});
var createGreeting = function(message, name) {
  return message + name;
};
var arrowGreeting = (function(message, name) {
  return message + name;
});
var arrowGreeting = (function(message, name) {
  return message + name;
});
var deliveryBoy = {
  name: "Santiago",
  handleMessage: function(message, handler) {
    handler(message);
  },
  receive: function() {
    this.handleMessage("hello, ", (function(message) {
      return console.log(message + "santiago");
    }));
  }
};
deliveryBoy.receive();
function greet(message) {
  var name = arguments[$traceurRuntime.toProperty(1)] !== (void 0) ? arguments[$traceurRuntime.toProperty(1)] : "santiago";
  console.log(message + ", " + name);
}
greet("hello", "primo");
