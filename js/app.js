//uso de class

class Greeter {
    constructor(message) {
        this.message = message;
    }

    greet() {
        var element = document.querySelector('#message');
        element.innerHTML = this.message;
    }
};

var greeter = new Greeter('Hola, Mundo!');
greeter.greet();


//uso de let

//En este ejemplo el resultado sera 10 10 10 10...

var fs = [];

for (var i = 0; i < 10; i++) {
    fs.push(function() {
        console.log(i);
    });
};

fs.forEach(function(f) {
    f();
})

// En este ejemplo al usar "let" es diferente...
var fs = [];

for (let i = 0; i < 10; i++) {
    fs.push(function() {
        console.log(i);
    });
};

fs.forEach(function(f) {
    f();
});


//arrows

var createGreeting = function(message, name) {
    return message + name;
}

var arrowGreeting = (message, name) => {
    return message + name;
}

var arrowGreeting = (message, name) => message + name;

var deliveryBoy = {

    name: "Santiago",

    handleMessage: function(message, handler) {
        handler(message);
    },

    receive: function() {
        this.handleMessage("hello, ", message => console.log(message + "santiago"));
    }
}

deliveryBoy.receive();


//custom parameters basic

function greet(message, name = "santiago") {
    console.log(message + ", " + name);
}

greet("hello", "primo");
